package com.example.garwan;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import com.example.garwan.endpoint.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class IntegrationTest {

    @LocalServerPort
    private int randomServerPort;

    private final TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void userTest() {
        User user = new User();
        user.setUsername("u");
        user.setEmail("e");
        String url = String.format("http://localhost:%d/api/users/", this.randomServerPort);

        // user is created
        ResponseEntity<User> response = this.testRestTemplate.postForEntity(url, user, User.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

        // same user cannot be created again
        response = this.testRestTemplate.postForEntity(url, user, User.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.CONFLICT));
    }

}
