package com.example.garwan;

import com.example.garwan.data.entity.AnimalCategoryEntity;
import com.example.garwan.data.entity.ImageEntity;
import com.example.garwan.data.entity.ProductEntity;
import com.example.garwan.data.entity.UserEntity;
import com.example.garwan.data.repository.AnimalCategoryRepository;
import com.example.garwan.data.repository.ImageRepository;
import com.example.garwan.data.repository.ProductRepository;
import com.example.garwan.data.repository.UserRepository;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GarwanApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AnimalCategoryRepository animalCategoryRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    void contextLoads() {
    }

    @Test
    @Transactional
    void userRepoTest() {
        this.userRepository.save(new UserEntity().setUsername("user").setEmail("email"));
        List<UserEntity> all = this.userRepository.findAll();
        Assert.assertEquals(1, all.size());
    }

    @Test
    @Transactional
    void productTest() {
        AnimalCategoryEntity dog = new AnimalCategoryEntity().setName("dog1");
        AnimalCategoryEntity cat = new AnimalCategoryEntity().setName("cat1");
        this.animalCategoryRepository.saveAll(List.of(
            dog,
            cat,
            new AnimalCategoryEntity().setName("others1")
        ));
        ImageEntity image1 = new ImageEntity().setUrl("a1");
        this.imageRepository.saveAll(List.of(
            image1,
            new ImageEntity().setUrl("b1")
        ));

        ProductEntity product = new ProductEntity().setName("name1").setImages(Set.of(image1))
            .setAnimalCategories(Set.of(dog, cat));
        this.productRepository.save(product);
    }

}
