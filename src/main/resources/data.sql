insert into user_entity (email, username, id) values ('user', 'user', '4f21f1c8aec4417a9f9ae8277674fbc0')

insert into animal_category_entity (name, id) values ('dog', 'd31e5aae4bd9490182a3a1acc7e29622')
insert into animal_category_entity (name, id) values ('cat','954d41dbad1541beb1c9a34e7e75e909')
insert into animal_category_entity (name, id) values ('other', '5c0c34cfb28d4d409731c133a83c3281')

insert into image_entity (url, id) values ('url1', 'ded927f592a54421a6e7088a3183bdf9')
insert into image_entity (url, id) values ('url2', 'dad1bc21c00d414589e7f575d6a96ead')

insert into product_entity (description, name, price, id) values ('desc1', 'prod1', 12.34, 'cd485ed81479431197aea91dbf583a61')
insert into product_entity (description, name, price, id) values ('desc2', 'pra2', 2.34, '65ef8c691c234d8e8a1300588bbce9f5')
insert into product_entity (description, name, price, id) values ('desc3', 'prod3', 42, 'f705194a71d04c34beec0978e0d3b9da')

insert into product_animalcategory (product_id, animalcategory_id) values ('cd485ed81479431197aea91dbf583a61', 'd31e5aae4bd9490182a3a1acc7e29622')
insert into product_animalcategory (product_id, animalcategory_id) values ('cd485ed81479431197aea91dbf583a61', '954d41dbad1541beb1c9a34e7e75e909')
insert into product_image (product_id, image_id) values ('cd485ed81479431197aea91dbf583a61', 'ded927f592a54421a6e7088a3183bdf9')

insert into product_animalcategory (product_id, animalcategory_id) values ('65ef8c691c234d8e8a1300588bbce9f5', 'd31e5aae4bd9490182a3a1acc7e29622')
insert into product_image (product_id, image_id) values ('65ef8c691c234d8e8a1300588bbce9f5', 'dad1bc21c00d414589e7f575d6a96ead')

insert into product_animalcategory (product_id, animalcategory_id) values ('f705194a71d04c34beec0978e0d3b9da', '5c0c34cfb28d4d409731c133a83c3281')