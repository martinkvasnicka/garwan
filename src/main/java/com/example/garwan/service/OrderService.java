package com.example.garwan.service;

import com.example.garwan.data.entity.OrderEntity;
import com.example.garwan.data.entity.OrderEntity_;
import com.example.garwan.data.entity.ProductOrderEntity;
import com.example.garwan.data.repository.OrderRepository;
import com.example.garwan.endpoint.model.Order;
import com.example.garwan.mapping.OrderMapper;
import com.example.garwan.security.CustomUser;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {

    private final OrderRepository orderRepository;

    private final ProductService productService;

    private final OrderMapper orderMapper;

    public Order createOrder(Map<UUID, Integer> products, CustomUser user) {
        List<ProductOrderEntity> productOrderEntities = products.keySet().stream()
            .map(this.productService::getProductDetails)
            .map(product ->
                new ProductOrderEntity()
                    .setPrice(product.getPrice())
                    .setCount(products.get(product.getId()))
            ).collect(Collectors.toList());

        OrderEntity orderEntity = new OrderEntity()
            .setCreatedBy(user.getUserId())
            .setTotalPrice(productOrderEntities.stream().mapToDouble(poe -> poe.getPrice() * poe.getCount()).sum())
            .setProducts(productOrderEntities)
            .setTime(new Date());
        productOrderEntities.forEach(poe -> poe.setOrder(orderEntity));
        return this.orderMapper.mapEntityToModel(this.orderRepository.save(orderEntity));
    }

    public List<Order> getUserOrders(UUID userId) {
        return this.orderMapper
            .mapEntitiesToModel(
                this.orderRepository.findAll((root, query, cb) -> cb.equal(root.get(OrderEntity_.createdBy), userId)));
    }
}
