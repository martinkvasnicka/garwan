package com.example.garwan.service;

import com.example.garwan.data.entity.ProductEntity;
import com.example.garwan.data.entity.ProductEntity_;
import com.example.garwan.data.repository.ProductRepository;
import com.example.garwan.endpoint.model.Product;
import com.example.garwan.exception.DataNotFoundException;
import com.example.garwan.mapping.ProductMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
@Transactional
public class ProductService {

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    public List<Product> findProductsWithoutDetails(int pageNo, Double min, Double max, String namePrefix) {
        Specification<ProductEntity> specification = buildProductSearchCriteria(min, max, namePrefix);
        Page<ProductEntity> page = this.productRepository.findAll(specification, PageRequest.of(pageNo, 2));
        return this.productMapper.mapEntitiesToModelWithoutDetails(page.toList());
    }

    public Product getProductDetails(UUID id) {
        return this.productMapper
            .mapEntityToModel(this.productRepository.findById(id).orElseThrow(DataNotFoundException::new));
    }

    public List<Product> findAll() {
        return this.productMapper.mapEntitiesToModel(this.productRepository.findAll());
    }

    /**
     * Create JPA specification with optional parts.
     *
     * @param min
     * @param max
     * @param namePrefix
     * @return
     */
    private Specification<ProductEntity> buildProductSearchCriteria(Double min, Double max, String namePrefix) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (min != null) {
                predicates.add(cb.greaterThan(root.get(ProductEntity_.price), min));
            }
            if (max != null) {
                predicates.add(cb.lessThan(root.get(ProductEntity_.price), max));
            }
            if (!StringUtils.isEmpty(namePrefix)) {
                predicates.add(
                    cb.like(cb.lower(root.get(ProductEntity_.name)), String.format("%s%%", namePrefix.toLowerCase())));
            }
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
