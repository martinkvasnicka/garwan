package com.example.garwan.service;

import com.example.garwan.data.entity.UserEntity;
import com.example.garwan.data.repository.UserRepository;
import com.example.garwan.endpoint.model.User;
import com.example.garwan.exception.DataNotFoundException;
import com.example.garwan.exception.UserNameExistException;
import com.example.garwan.mapping.UserMapper;
import java.util.Optional;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    public void createUser(String username, String email) {
        getOptionalUser(username).ifPresentOrElse(u -> {
                throw new UserNameExistException();
            },
            () -> this.userRepository.save(new UserEntity().setEmail(email).setUsername(username)));
    }

    public User findUser(String username) {
        return this.userMapper.mapEntityToModel(getUser(username));
    }

    public User getUserWithDetails(String username) {
        return this.userMapper.mapEntityToModelWithId(getUser(username));
    }

    private UserEntity getUser(String username) {
        return getOptionalUser(username).orElseThrow(DataNotFoundException::new);
    }

    private Optional<UserEntity> getOptionalUser(String username) {
        return this.userRepository.findUserEntityByUsername(username);
    }
}
