package com.example.garwan.endpoint.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class User {

    @JsonIgnore
    private UUID id;

    @NotBlank
    private String email;

    @NotBlank
    private String username;
}
