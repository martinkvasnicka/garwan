package com.example.garwan.endpoint.model;

import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class Image {

    @NotNull
    private String url;

}
