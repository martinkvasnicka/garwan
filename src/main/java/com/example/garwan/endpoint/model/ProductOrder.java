package com.example.garwan.endpoint.model;

import java.util.UUID;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class ProductOrder {

    private UUID id;

    private double price;

    private int count;
}
