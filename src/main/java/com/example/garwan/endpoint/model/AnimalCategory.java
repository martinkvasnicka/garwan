package com.example.garwan.endpoint.model;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class AnimalCategory {

    private String name;
}
