package com.example.garwan.endpoint.model;

import java.util.Date;
import java.util.List;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class Order {
    
    private double totalPrice;

    private List<ProductOrder> products;

    private Date time;
}
