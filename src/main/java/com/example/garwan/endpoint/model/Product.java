package com.example.garwan.endpoint.model;

import java.util.List;
import java.util.UUID;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class Product {

    private UUID id;

    private String name;

    private List<AnimalCategory> animalCategories;

    private double price;

    private String description;

    private List<Image> gallery;
}
