package com.example.garwan.endpoint.rest;

import com.example.garwan.endpoint.model.User;
import com.example.garwan.service.UserService;
import io.swagger.annotations.ApiOperation;
import javax.transaction.Transactional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Transactional
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "Login user")
    @GetMapping(value = "/{name}", produces = {"application/json"})
    public ResponseEntity<User> loginUser(@PathVariable("name") String name) {
        User user = this.userService.findUser(name);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @ApiOperation(value = "Create user")
    @PostMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        this.userService.createUser(user.getUsername(), user.getEmail());
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
