package com.example.garwan.endpoint.rest;

import com.example.garwan.endpoint.model.Order;
import com.example.garwan.security.CustomUser;
import com.example.garwan.service.OrderService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
@Transactional
public class OrderController {

    private final OrderService orderService;

    @ApiOperation(value = "Create order")
    @PostMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<Order> createOrder(@RequestBody Map<UUID, Integer> products, Authentication authentication) {
        Order order = this.orderService.createOrder(products, (CustomUser) authentication.getPrincipal());
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Find all user orders")
    @GetMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<List<Order>> findUserOrders(Authentication authentication) {
        List<Order> result = this.orderService.getUserOrders(((CustomUser) authentication.getPrincipal()).getUserId());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
