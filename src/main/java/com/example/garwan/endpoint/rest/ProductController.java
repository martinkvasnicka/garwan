package com.example.garwan.endpoint.rest;

import com.example.garwan.endpoint.model.Product;
import com.example.garwan.service.ProductService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
@Transactional
public class ProductController {

    private final ProductService productService;

    @ApiOperation(value = "Product details")
    @GetMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<Product> getProduct(@PathVariable("id") UUID id) {
        Product product = this.productService.getProductDetails(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @ApiOperation(value = "Find products without details")
    @GetMapping(value = "/noDetails", produces = {"application/json"})
    public ResponseEntity<List<Product>> findProducts(@RequestParam("pageNo") int pageNo,
        @RequestParam(value = "min", required = false) Double min,
        @RequestParam(value = "max", required = false) Double max,
        @RequestParam(value = "namePrefix", required = false) String namePrefix) {
        List<Product> result = this.productService.findProductsWithoutDetails(pageNo, min, max, namePrefix);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Find all products")
    @GetMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<List<Product>> findProducts() {
        List<Product> result = this.productService.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
