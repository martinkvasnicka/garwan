package com.example.garwan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GarwanApplication {

    public static void main(String[] args) {
        SpringApplication.run(GarwanApplication.class, args);
    }
}
