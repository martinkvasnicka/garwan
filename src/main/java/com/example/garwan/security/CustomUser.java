package com.example.garwan.security;

import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUser extends User {

    @Getter
    private final UUID userId;

    public CustomUser(com.example.garwan.endpoint.model.User user) {
        super(user.getUsername(), user.getUsername(), Set.of(new SimpleGrantedAuthority("ROLE_USER")));
        this.userId = user.getId();
    }
}
