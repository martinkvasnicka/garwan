package com.example.garwan.security;

import com.example.garwan.exception.DataNotFoundException;
import com.example.garwan.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        try {
            return new CustomUser(this.userService.getUserWithDetails(username));
        } catch (DataNotFoundException ex) {
            throw new UsernameNotFoundException(username);
        }
    }
}
