package com.example.garwan.data.repository;

import com.example.garwan.data.entity.ProductOrderEntity;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductOrderRepository extends JpaRepository<ProductOrderEntity, UUID> {

}
