package com.example.garwan.mapping;

import com.example.garwan.data.entity.ProductOrderEntity;
import com.example.garwan.endpoint.model.ProductOrder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProductOrderMapper {

    ProductOrder mapEntityToModel(ProductOrderEntity entity);
}
