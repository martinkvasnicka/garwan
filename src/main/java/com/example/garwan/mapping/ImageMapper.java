package com.example.garwan.mapping;

import com.example.garwan.data.entity.ImageEntity;
import com.example.garwan.endpoint.model.Image;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ImageMapper {

    Image mapEntityToModel(ImageEntity entity);
}
