package com.example.garwan.mapping;

import com.example.garwan.data.entity.ProductEntity;
import com.example.garwan.endpoint.model.Product;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {
    AnimalCategoryMapper.class, ImageMapper.class})
public interface ProductMapper {

    @Mapping(source = "images", target = "gallery")
    Product mapEntityToModel(ProductEntity entity);

    @Mapping(target = "description", ignore = true)
    @Mapping(target = "gallery", ignore = true)
    Product mapEntityToModelWithoutDetails(ProductEntity entity);

    default List<Product> mapEntitiesToModelWithoutDetails(List<ProductEntity> entities) {
        return entities.stream().map(this::mapEntityToModelWithoutDetails).collect(Collectors.toList());
    }

    default List<Product> mapEntitiesToModel(List<ProductEntity> entities) {
        return entities.stream().map(this::mapEntityToModel).collect(Collectors.toList());
    }
}
