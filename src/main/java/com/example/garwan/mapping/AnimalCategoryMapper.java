package com.example.garwan.mapping;

import com.example.garwan.data.entity.AnimalCategoryEntity;
import com.example.garwan.endpoint.model.AnimalCategory;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AnimalCategoryMapper {

    AnimalCategory mapEntityToModel(AnimalCategoryEntity entity);
}
