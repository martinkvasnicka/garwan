package com.example.garwan.mapping;

import com.example.garwan.data.entity.UserEntity;
import com.example.garwan.endpoint.model.User;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    User mapEntityToModel(UserEntity entity);

    User mapEntityToModelWithId(UserEntity entity);
}
