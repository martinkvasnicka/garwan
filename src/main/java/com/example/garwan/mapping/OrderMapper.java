package com.example.garwan.mapping;

import com.example.garwan.data.entity.OrderEntity;
import com.example.garwan.endpoint.model.Order;
import java.util.List;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = ProductOrderMapper.class)
public interface OrderMapper {

    Order mapEntityToModel(OrderEntity entity);

    List<Order> mapEntitiesToModel(List<OrderEntity> entities);
}
